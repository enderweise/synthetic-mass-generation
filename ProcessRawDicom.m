function [img16,hdr] = ProcessRawDicom(varargin)

if nargin == 1  % input is filename
    filepath = char(varargin(1));
    if isa(filepath,'char')
        try 
            img16 = dicomread(filepath);
            hdr = dicominfo(filepath);
        catch
            img16 = false;
            hdr = false;
            return;
        end
    else
        img16 = false;
        hdr = false;
        return;
    end
elseif nargin == 2  % inputs are image and header
    img16 = cell2mat(varargin(1));
    hdr = cell2struct(varargin(2),{'dummy'},1);
    hdr = hdr.dummy;
    if ( isa(hdr,'struct')  &&  isa(img16,'uint16') )
    elseif ( isa(hdr,'struct')  &&  isa(img16,'double') )
        img16 = uint16(img16);
    elseif ( isa(hdr,'double')  &&  isa(img16,'struct') )
        dummy = img16;  img16 = uint16(hdr);  hdr = dummy;
    elseif ( isa(hdr,'uint16')  &&  isa(img16,'struct') )
        dummy = img16;  img16 = hdr;  hdr = dummy;
    else
        img16 = false;
        hdr = false;
        disp('Incorrect format(s) for inputs to ProcessRawDicom()')
        return;
    end
else
    disp('Incorrect number of inputs for ProcessRawDicom()')
    return;
end


if conditions(hdr)
    % sigmoid function:                g_max - g_min
    %                     S[x] =    ------------------    + g_min
    %                                1+exp(-4(x-A)/B)
    %
    g_max = 4096; % = 2^12
    % assume g_min = 0;
    % A and B correspond to the 'window center' and 'window width' (respectively). These may be defined in the dicom tags, but this script calculates A and B.
    
    
    %------------------------- Prepare Image ------------------------------
    % invert image (usually needed)
    try invert_tf = strcmp(hdr.PresentationLUTShape,'INVERSE');
    catch
        invert_tf = true; % assume raw dicom is not inverted
    end
    if invert_tf
        [img16,hdr,~,~] = InvertPixelData(img16,hdr);
    end
    % create double version of image for easy computation
    imdbl = double(img16);
    % black out pixels not to be computed in imdbl
    breast = FindBreastProjection(imdbl,hdr);
    imdbl = imdbl.*breast;
    
    imdbl = NormBreastPhysics(imdbl,hdr);
    
    
    %----------------- Find Constants for Sigmoid Function ----------------
    % Create histogram (uint16)
    MaxVal_0 = uint16(max(max(imdbl)));
    histodat = zeros(MaxVal_0,1);
    for r = 1:size(imdbl,1)
        for c = 1:size(imdbl,2)
            if breast(r,c)
                histodat(uint16(imdbl(r,c))) = histodat(uint16(imdbl(r,c))) + 1;
            end
        end
    end
    
        % METHOD 1 - Map FWHM intersects to bottom and top
        % retired 2015-12-22
        % M_0 = max(histodat);
        % if isempty(M_0)
        %   return;
        % elseif max(M_0) == 0
        %   return;
        % elseif max(size(M_0)) > 1
        %   M_0 = sum(M_0)/max(size(M_0));
        % elseif max(size(M_0)) == 1
        % else
        %   disp('Cannot interperet histogram data. Locations of max values:')
        %   disp(M_0)
        % end
        %
        % HM = M_0/2; % half max value
        % b = double(find(histodat>HM,1,'last')); % old histogram upper intersect with Half Max
        % a = double(find(histodat>HM,1,'first')); % old histogram lower intersect with Half Max
    
    % METHOD 2 - Find points a & b where area under histogram equals certain
    % amount. map these points to top and bottom
    stop1 = 0.1;
    stop2 = 0.8;
    
%     top = 2000; % point to which larger histogram delineation maps to.
%     bottom = 1000; % same but for smaller histogram delineation.
 % these values were used until Jan 12, 2016
 
    top = 1500;
    bottom = 550;
    
    total_area = sum(histodat);
    
    stop1 = stop1* total_area;
    summation = 0;
    a = 0;
    while summation < stop1
        a = a + 1;
        summation = summation + histodat(a);
    end
    
    b = a;
    stop2 = stop2* total_area;
    while summation < stop2
        b = b + 1;
        summation = summation + histodat(b);
    end
    
    % determine sigmoid constants for the image:
    B = 4*(b-a)/(log(top*(g_max-bottom)/bottom/(g_max-top))); % Window Width
    A = B*log((g_max-top)/top)/4+b; % Window Center
%     disp(sprintf('  A: %d',A'))
%     disp(sprintf('  B: %d',B'))
    
    
    %---------------------- Apply Sigmoid Function ------------------------
    img16(:,:) = uint16(0);
    for r = 1:size(img16,1)
        for c = 1:size(img16,2)
            if imdbl(r,c) ~= 0 
                img16(r,c) = sigmoid(imdbl(r,c),g_max,A,B);
            end
        end
    end
    
    %**********************************************************************
%     MaxVal_0 = max(max(img16));
%     histodat = zeros(MaxVal_0,1);
% 
%     for r = 1:size(img16,1)
%       for c = 1:size(img16,2)
%         if img16(r,c) > 0
%           histodat(img16(r,c)) = histodat(img16(r,c)) + 1;
%         end
%       end
%     end
%     
%     h = figure('visible','off');
%     histogram(img16);
%     axis([1 MaxVal_0 0 40000]);
%     total_area = sum(histodat);
%     curr_area = 0;
%     q = 0.1;
%     for l = 1:max(size(histodat))
%       curr_area = curr_area + histodat(l);
%       if curr_area >= q*total_area;
%         q = q + 0.1;
%         line([curr_area, curr_area],[0,40000]);
%       end
%     end
%     title(sprintf('for processing - noClahe'));
%     saveas(h,['DMISTraw/',sprintf('%d_Process_noClahe.png', index)]);
%     close(h);
    %**********************************************************************
    
    %--- apply Contrast Limited Adaptive Histogram Equalization (CLAHE) ---
    img16 = adapthisteq(img16,'NBins',2^12,'ClipLimit',0.003,'Distribution','exponential','Alpha',10,'NumTiles',[2,2]);

    %------------------ apply Gaussian Smoothing Filter -------------------
    img16 = imgaussfilt(img16);

    %----------- apply Edge Restoration function: UnSharp Mask ------------
    img16 = imsharpen(img16,'Radius',10,'Amount',1.5);
    
    % % Save Histogram
    % h = figure('visible','off');
    % bar(histodat);
    % title(sprintf('Area: %d',sum(histodat)));
    % hold on;
    % line([a,a],[0,max(histodat)]);
    % line([b,b],[0,max(histodat)]);
    % saveas(h,['output/',hdr.SOPInstanceUID,'_hist1.jpg']);
    % hold off
    % close(h);
    
    
    % Change hdr Info
    hdr.PixelIntensityRelationship = 'LOG';
    hdr.WindowCenter = num2str(uint16(A));
    hdr.WindowWidth = num2str(uint16(B));
    hdr.PresentationIntentType = 'FOR PRESENTATION';
    hdr.SOPClassUID = '1.2.840.10008.5.1.4.1.1.1.2';
    % change SOPClassUID from '1.2.840.10008.5.1.4.1.1.1.2.1' (for processing)
    %                    to   '1.2.840.10008.5.1.4.1.1.1.2' (for presentation)
    hdr.MediaStorageSOPClassUID = '1.2.840.10008.5.1.4.1.1.1.2';
    % needs to be the same as SOPClassUID
    hdr.BitsAllocated = uint16(12);
    hdr.BitDepth = uint16(12);
    hdr.BitsStored = uint16(12);
    hdr.PixelIntensityRelationshipSign = -1;

else % not a raw dicom
    img16 = false;
    hdr = false;
end

end

%% --------------------------------------------------------------------------

function y = sigmoid(x,g_max,A,B)

if x == 0
    y = uint16(0);
else
    y = uint16(g_max/(1+exp(4*(A-x)/B)));
end

end

%% --------------------------------------------------------------------------

function cont_tf = conditions(hdr)

try
    cont_tf = ( strcmp(hdr.PresentationIntentType,'FOR PROCESSING')  ||  strcmp(hdr.PresentationIntentType,'For Processing') );
catch ME
    cont_tf = false;
end

end
