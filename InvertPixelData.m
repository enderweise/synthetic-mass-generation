function [img,hdr,bd,maxpixelval] = InvertPixelData(img,hdr)

  %find the maximum pixel depth
  bd=0;
  try bd = hdr.BitDepth;
  catch ME
    try bd = hdr.BitsStored;
    catch ME
      m = double(max(max(img)));
      bd = 0;
      while m > 1
        m=m/2;
        bd=bd+1;
      end
    end
  end
  
  if bd == 0
    warning('Could not invert image because bit depth was not found. Returning un-inverted image')
    return;
  end
  
  if isa(img,'double')
    maxpixelval = double(2^bd)-1;
  elseif isa(img,'uint16')
    maxpixelval = uint16(2^bd)-1;
  end
  
  % invert the pixel values
  img = maxpixelval - img;
  
  % change the header so that images aren't inverted multiple times
  if strcmp(hdr.PresentationLUTShape,'INVERSE');
    hdr.PresentationLUTShape = 'IDENTITY';
  elseif strcmp(hdr.PresentationLUTShape,'IDENTITY');
    hdr.PresentationLUTShape = 'INVERSE';
  else
    input(['Presentation Intent Type was "',hdr.PresentationIntentType,'" before inversion. Enter new value to save in header:'],'s');
  end

end
