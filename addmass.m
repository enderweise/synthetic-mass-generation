clc; clear; close all;

% Change to directory where crop mat files are located
folder = 'C:\Users\Behnam\Desktop\synthetic-mass-generation\compiledCrops\';
cropdir = dir(folder);

while(1)
    % Pick random crop
    index = round(size(cropdir,1)*randn);
    if (index > size(cropdir,1)|| index < 3)
        continue;
    end
    mass = load([folder cropdir(index).name]);
    mass = mass.tissueHeight;
    
    % Choose normal file
    [imgpath, pathname, ~] = uigetfile('\\FILESERVER\share\users\eric\DMIST\For_Processing_DICOMS\*.*');
    img = dicomread([pathname '\' imgpath]);
    hdr = dicominfo([pathname '\' imgpath]);
    
    massEmbedded = addMassHeightToNormalImage(img,hdr,mass,[cropdir(index).name '_'],imgpath);
    
end