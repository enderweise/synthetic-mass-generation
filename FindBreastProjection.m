function [breast,level] = FindBreastProjection(img,hdr)

% inputs:
    % img - DICOM image which is NOT a diagnostic image with clamps, etc.
    % hdr - the DICOM meta-information
% output:
    % double image same size as img which has 1 at locations of breast
    % tissue


try do_invert = strcmp(hdr.PresentationLUTShape,'INVERSE');
catch ME
    do_invert = false; % assume no inversion is needed, do nothing
end

if do_invert
  [img,~,bd,~] = InvertPixelData(img,hdr);
else
  try bd = hdr.BitDepth;
  catch ME
    try bd = hdr.BitsStored;
    catch ME
      m = double(max(max(img)));
      bd = 0;
      while m > 1
        m=m/2;
        bd=bd+1;
      end
    end
  end
end
%sprintf('Bit Depth: %d', bd) % troubleshooting

% Stability parameters
if bd == 14
    stability = 200;
elseif bd == 15
    stability = 400;
elseif bd == 16
    stability = 800;
elseif bd == 13
    stability = 100;
else
    stability = double(2^(bd-13))*100;
end
%sprintf('Stability: %d', stability) % troubleshooting


if sum(sum(find(img==0))) > 0.2*size(img,1)*size(img,2)
    [breast,level] = dilation(img,stability,bd);

else
    % Otsu's method
    [level,~] = graythresh(img);
    level = level*double(2^bd);
    
    if ( ~isnan(level)  &&  isfinite(level)  &&  level ~= 0 )
        breast = double(zeros(size(img)));
        dummy = breast;
        for r = 1:size(img,1)
            for c = 1:size(img,2)
                if img(r,c) > level
                    breast(r,c) = 1;
                elseif img(r,c) > level - stability;
                    dummy(r,c) = 1;
                end
            end
        end
        breast = double(bwareafilt(logical(breast),1));
        %   dummy = double(bwareafilt(logical(dummy),1));
%        figure(2); imshow(breast); % troubleshooting
%        disp('Otsu') % troubleshooting
%        sprintf('Level: %d', level) % troubleshooting
        
        % conditions to take image
        B = sum(sum(breast));
        D = sum(sum(dummy));
        % check the area condition, i.e. that not too much of the
        % image is assumed to be the breast area.
        if B > 0.9*size(img,1)*size(img,2)
%            disp('Start imdilate method') % troubleshooting
            [breast,level] = dilation(img,stability,bd);
        end
        
        % check stability condition, i.e. that the breast boundary
        % doesn't change significantly when the level is changed
        % slightly
        if B*0.001 < D
%            disp('Start imdilate method') % troubleshooting
            [breast,level] = dilation(img,stability,bd);
        end
        
        % if Otsu gives unuseable level then use imdilate method for
        % finding breast area
    else
%         disp('Start imdilate method') % troubleshooting
        [breast,level] = dilation(img,stability,bd);
    end
end

end

%%-------------------------------------------------------------------------
function [breast,level] = dilation(img,stability,bd)


if bd == 14
    levelvec = 500:500:16000;
    levelvec = [1,levelvec];
elseif bd == 15
    levelvec = 1000:1000:32000;
    levelvec = [1,levelvec];
elseif bd == 16
    levelvec = 2000:2000:64000;
    levelvec = [1,levelvec];
elseif bd == 13
    levelvec = 250:250:8000;
    levelvec = [1,levelvec];
else
    levelvec = 250:250:8000;
    levelvec = levelvec*double(2^(bd-13));
    levelvec = [1,levelvec];
end

H = size(img,1);
W = size(img,2);


size_error = 0;
for k = 1:size(levelvec,2)
    level = levelvec(k);
    breast = imdilate(double(img <= level),ones(15));
    breast = 1-breast;
    breast = double(bwareafilt(logical(breast),1));
    B = sum(sum(breast));
%     figure(2); imshow(breast)  % troubleshooting
%     sprintf('Level: %d', level) % troubleshooting
    
    dummy = imdilate(double(img <= level + stability),ones(15));
    dummy = 1-dummy;
    dummy = double(bwareafilt(logical(dummy),1));
    D = sum(sum(dummy));
    
    % total area condition
    if B < 0.7*H*W
        % stability condition
        if D > 0.97*B
            return;
        else
%             disp('unstable') % troubleshooting
        end
    elseif ( B < 0.9*H*W  &&  B > 0.7*H*W  &&  D > 0.97*B )
        size_error = size_error + 1;
%         disp('area too large') % troubleshooting
    end
    if size_error == 5
        level = levelvec(k-4);
%         disp('override large area criterion') % troubleshooting
%         sprintf('Level: %d', level) % troubleshooting
        breast = imdilate(double(img <= level),ones(15));
        breast = 1-breast;
        breast = double(bwareafilt(logical(breast),1));
%         figure(2); imshow(breast)  % troubleshooting
        return;
    end
end

end
