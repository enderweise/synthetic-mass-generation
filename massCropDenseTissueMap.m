function cropTissueHeight = massCropDenseTissueMap(img,header,xmin,xmax,ymin,ymax)

% function that takes as input a raw dicom image (with its header) and a bounding box of a mass
% within the image and as output gives an array with the height of the dense tissue at each 
% pixel within in the crop. 


% check to see if this dicom file should be analyzed
run_script = Conditions(header);

if run_script
  if ~isa(img,'double')
    img = double(img);
  end
  
  % get pixel information
%  try
%    pixarea = header.PixelSpacing(1)*header.PixelSpacing(2);
%  catch ME
%    try
%      pixarea = header.ImagerPixelSpacing(1)*header.ImagerPixelSpacing(2);
%    catch ME
%      pixarea = 0.0049; % assume that pixel spacing is isotropic and equal to 0.07 mm
%    end
%  end
  [isocurve,periphery,breast,interior,header] = IsointensityCurve(img,header);
  
  % calculate total Breast Volume
  breast_thickness_map = RawDicomThickness(periphery,breast,interior);
%  total_breast_volume = sum(sum(breast_thickness_map))*header.BodyPartThickness*pixarea;
  
  % calculate Glandular Tissue Volume
  fatty_tissue_intensity = sum(sum(img.*isocurve))/sum(sum(isocurve));
  cropTissueImage = img(ymin:ymax,xmin:xmax);
  cropTissueHeight = DenseTissueVolume(cropTissueImage,fatty_tissue_intensity,breast_thickness_map,header);
  
end % conditional: if run_script

end % function: massCropDenseTissueMap()


%% 0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0
function breast_thickness_map = BreastThickness(breast_thickness_map,breast,compressed_height,pixel_width)

% creates an array which gives the thickness of the breast at each pixel

% Calculate the height of the breast based on distance from the edge of the breast
for row = 1:size(breast_thickness_map,1)
  for col = 1:size(breast_thickness_map,2)
    if breast(row,col)
      if breast_thickness_map(row,col) >= compressed_height/2
        breast_thickness_map(row,col) = compressed_height;
      elseif breast_thickness_map(row,col) < compressed_height/2
        breast_thickness_map(row,col) = 2*sqrt((compressed_height/2)^2-(compressed_height/2-breast_thickness_map(row,col))^2);
      end % conditional
    end % conditional
  end % loop over columns
end % loop over rows

end % function: BreastThickness


%% 0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0
function continue_tf = Conditions(header)

continue_tf = true;

if continue_tf
  
  % grab relevant information from the dicom header
  try
    anode_material = header.AnodeTargetMaterial; % currently have information for Mo and Rh
    if ( ~strcmp(anode_material,'MOLYBDENUM') && ~strcmp(anode_material,'RHODIUM') )
      continue_tf = false;
      sprintf(['Unknown Anode Target Material ',anode_material,' for patient ',header.PatientID,' ',header.ImageLaterality,header.ViewPosition,' AccessionNumber ',header.AccessionNumber]);
    end
  catch ME
    continue_tf = false;
    try sprintf(['Non-existent field Anode Target Material for patient ',header.PatientID,' ',header.ImageLaterality,header.ViewPosition,' AccessionNumber ',header.AccessionNumber]);
    catch ME
    end
  end
    
  
  try
    filter_thickness = (header.FilterThicknessMaximum+header.FilterThicknessMinimum)/2; % units = mm
    % currently have information for filter thicknesses of 30 micrometers
    if filter_thickness ~= 0.03
      continue_tf = false;
      sprintf(['Unsupported Filter Thickness ',num2str(filter_thickness),' for patient ',header.PatientID,' ',header.ImageLaterality,header.ViewPosition,' AccessionNumber ',header.AccessionNumber]);
    end
  catch ME
    %continue_tf = false;
    try sprintf(['Non-existent field Filter Thickness for patient ',header.PatientID,' ',header.ImageLaterality,header.ViewPosition,' AccessionNumber ',header.AccessionNumber]);
    catch ME
    end
  end
  
  
  try
    filter_material = header.FilterMaterial; % currently have information for Mo and Rh
    if ( ~strcmp(filter_material,'MOLYBDENUM') && ~strcmp(filter_material,'RHODIUM') )
      continue_tf = false;
      sprintf(['Unknown Filter Material ',filter_material,' for patient ',header.PatientID,' ',header.ImageLaterality,header.ViewPosition,' AccessionNumber ',header.AccessionNumber]);
    end
  catch ME
    continue_tf = false;
    try  sprintf(['Non-existent field Filter Material for patient ',header.PatientID,' ',header.ImageLaterality,header.ViewPosition,' AccessionNumber ',header.AccessionNumber]);
    catch ME
    end
  end
  
  
  try
    tube_voltage = round(header.KVP); % Volts, currently have information for voltages from 24-32
    if ( tube_voltage > 32 || tube_voltage < 24 )
      continue_tf = false;
      sprintf(['Unsupported Max Tube Voltage ',num2str(tube_voltage),' for patient ',header.PatientID,' ',header.ImageLaterality,header.ViewPosition,' AccessionNumber ',header.AccessionNumber]);
    end
  catch ME
    continue_tf = false;
    try sprintf(['Non-existent field Max Tube Voltage for patient ',header.PatientID,' ',header.ImageLaterality,header.ViewPosition,' AccessionNumber ',header.AccessionNumber]);
    catch ME
    end
  end
  
  
  try
    breast_thickness = header.BodyPartThickness; % units = mm
    if ( breast_thickness < 15 || breast_thickness >= 95 )
      continue_tf = false;
      sprintf(['Unsupported Breast Thickness ',num2str(breast_thickness),' for patient ',header.PatientID,' ',header.ImageLaterality,header.ViewPosition,' AccessionNumber ',header.AccessionNumber]);
    end
  catch ME
    continue_tf = false;
    try sprintf(['Non-existent Field Breast Thickness for patient ',header.PatientID,' ',header.ImageLaterality,header.ViewPosition,' AccessionNumber ',header.AccessionNumber]);
    catch ME
    end
  end
  
end

end


%% 0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0
function img = CropImage(img,minrow,maxrow,mincol,maxcol)

% Takes an image and four indices as input. Any rows with indices less than
% minrow or greater than maxrow are removed, and the same is done for 
% columns.

if( minrow < maxrow  &&  mincol < maxcol )
  img = img(minrow:maxrow,mincol:maxcol);
end

end


%% 0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0
function cropArray = DenseTissueVolume(tissueImage,fatPixelIntensity,breast_thickness_map,header)


% Calculates the total volume of the dense tissue a dicom image given 
% the image, a binary map of the breast tissue, and the dicom header.
% 
% The output of this script is the dense tissue volume in mm^3
%
% This program follows the method used by Engeland et al. in "Volumetric 
% Breast Density Estimation from Full-Field Digital Mammograms", IEEE
% Transactions on Medical Imaging, vol. 25, no. 3, March 2006.
  
% Grab relevant information from the dicom header
anode_material = header.AnodeTargetMaterial;
filter_material = header.FilterMaterial;
tube_voltage = round(header.KVP);
breast_thickness = header.BodyPartThickness; % units = mm
breast_thickness = round(breast_thickness/10); % convert breast thickness to cm and round  
  
  % retrieve the correct attenuation coefficients from the tables in this file --RETIRED April 4, 2016
  if ( strcmp(filter_material,'MOLYBDENUM')  &&  strcmp(anode_material,'MOLYBDENUM') )
    mu = csvread('tables/MoMo.csv',tube_voltage-24,breast_thickness-2,[tube_voltage-24,breast_thickness-2,tube_voltage-24,breast_thickness-2]);
  end

  if ( strcmp(filter_material,'RHODIUM')  &&  strcmp(anode_material,'MOLYBDENUM') )
    mu = csvread('tables/MoRh.csv',tube_voltage-24,breast_thickness-2,[tube_voltage-24,breast_thickness-2,tube_voltage-24,breast_thickness-2]);
  end
  
  if ( strcmp(filter_material,'RHODIUM')  &&  strcmp(anode_material,'RHODIUM') )
    mu = csvread('tables/RhRh.csv',tube_voltage-24,breast_thickness-2,[tube_voltage-24,breast_thickness-2,tube_voltage-24,breast_thickness-2]);
  end

%  % retrieve the correct attenuation coefficients for the mammogram setup:
%  load('adiposeAttenuationCoeff.mat');
%  kvp = header.KVP;
%  i = find(double(kvp < Voltage),1);
%  if (kvp - Voltage(i-1)) < (Voltage(i) - kvp)
%      i = i - 1;
%  end
%  mu = lin_atten_coeff(i);
  
  % Integrate over the breast projection according to Engeland et al.
  cropArray = zeros(size(tissueImage));
  for row  =  1:size(tissueImage,1)-1
    for col = 1:size(tissueImage,2)-1
      if tissueImage(row,col) < fatPixelIntensity % here I am assuming that any pixel with intensity greater than the fatPixelIntensity is entirely fatty. This should be checked for validity.
        cropArray(row,col) = -log(double(tissueImage(row,col)/fatPixelIntensity))/mu;
      end
    end
  end
  
%   % Used for troubleshooting only when relatively few dicoms are being
%   % analyzed (i.e. <10)
%   % Save a jpg marking the minimum pixel and its value
%   breast_tissue_img(minpixelrow-1:minpixelrow+1,1:minpixelcol-25) = m;
%   breast_tissue_img(minpixelrow-1:minpixelrow+1,minpixelcol+25:size(breast_tissue_img,2)) = m;
%   breast_tissue_img(1:minpixelrow-25,minpixelcol-1:minpixelcol+1) = m;
%   breast_tissue_img(minpixelrow+25:size(breast_tissue_img,1),minpixelcol-1:minpixelcol+1) = m;
%   t = char(datetime('now','TimeZone','local','Format','h-m-s'));
%   imwrite(double(breast_tissue_img)/5000,['output/','fatPixelIntensity_',num2str(fatPixelIntensity),'_',header.AccessionNumber,'_',header.ImageLaterality,header.ViewPosition,'_',num2str(t),'.jpg']);

end


%% 0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0
function [minval,minrowcent,mincolcent] = FindFattyTissue(breast_tissue_img,tissue_projection_map,w,a,b,c)

% This function searches a mammogram for the fattiest region. The regions
% that are searched are squares with edge lengths of 2*w+1 (pixels), and 
% are placed on an orthogonal grid with a distance of w pixels between the 
% centers of neighboring squares.


nopix = (2*w+1)^2;


% set initial values
row_center = w + 2;
col_center = size(breast_tissue_img,2)-w-1;
minval = 1000000;
minrowcent = row_center;
mincolcent = col_center;


% search the whole image for regions which have a lower average intensity
while row_center + w <= size(breast_tissue_img,1)
  while col_center-w >= 1
    if prod(prod(tissue_projection_map(row_center-w:row_center+w,col_center-w:col_center+w)))
      compval = sum(sum(breast_tissue_img(row_center-w:row_center+w,col_center-w:col_center+w)))/nopix;
      if compval < minval
        minval = compval;
        minrowcent = row_center;
        mincolcent = col_center;
      end
    end
    col_center = col_center - w;
  end
  col_center = size(breast_tissue_img,2)-w-1;
  row_center = row_center + w;
end

% for testing and troubleshooting purposes, save the images with least
% dense region marked
m = max(max(breast_tissue_img));
try
%  breast_tissue_img(minrowcent-w-3:minrowcent-w,mincolcent-w-3:mincolcent+w+3) = m;
 % breast_tissue_img(minrowcent+w:minrowcent+w+3,mincolcent-w-3:mincolcent+w+3) = m;
  %breast_tissue_img(minrowcent-w-3:minrowcent+w+3,mincolcent-w-3:mincolcent-w) = m;
  %breast_tissue_img(minrowcent-w-3:minrowcent+w+3,mincolcent+w:mincolcent+w+3) = m;
  %imwrite(double(breast_tissue_img)/5000,['output/',a,'_',b,c,'_AvgMinVal_',num2str(minval),'.jpg']);
catch ME
end

end


%% 0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0
function [dicom_image,breast, distance_to_edge_map] = ProjectedBreastArea(dicom_image,view_position,pixel_width)

% this is a function taking a DICOM mammogram image and its view position and returns a
% binary map % of the image with ones in the pixels which contain the projected breast image.


% create binary map where 0 represents breast tissue and 1 is background
breast = imdilate(double(dicom_image==0),ones(15));

% invert the area map to represent the volume profile of the breast
for i = 1:size(breast,1)
  for j = 1:size(breast,2)
    if breast(i,j) == 1
      breast(i,j) = 0;
    elseif breast(i,j) == 0
      breast(i,j) = 1;
    end
  end
end


% find the breast boundary
breast_edge_map = edge(breast);

distance_to_edge_map = bwdist(breast_edge_map);

% zero out distances outside breast area
for i = 1:size(distance_to_edge_map,1)
  for j = 1:size(distance_to_edge_map,2)
    distance_to_edge_map(i,j) = distance_to_edge_map(i,j)*breast(i,j);
  end
end

%convert distance map to units of mm
distance_to_edge_map = distance_to_edge_map*pixel_width;



% Take out extraneous structures, implants, muscle, etc

% Search for breast implant
implant_boundary = imdilate(double(dicom_image>3500),ones(15));
cc = bwconncomp(implant_boundary);
if cc.NumObjects > 0
  numPixels = cellfun(@numel,cc.PixelIdxList);
  [region_area,idx] = max(numPixels);
  breast_projection_area = sum(sum(breast));
  if ( region_area/breast_projection_area > 0.2  &&  region_area/breast_projection_area < 0.7 )
    breast(cc.PixelIdxList{idx}) = 0;
  end
end 


% if the image is an MLO then find the muscle boundary
if strcmp(view_position,'MLO')
  [xy max_angl min_angl] = muscle_finder(dicom_image);
  %the following 4 assignments assumes that the line slopes down and to
  %the right on the image.
  if ( size(xy,1) == 2  &&  size(xy,2) == 2 )
  ymax = max(xy(1,1),xy(1,2));
  ymin = min(xy(1,1),xy(1,2));
  xmax = max(xy(2,1),xy(2,2));
  xmin = min(xy(2,1),xy(2,2));
  
  
  delta = (xmax-xmin)/(ymax-ymin);
  imwidth = size(dicom_image,2);
  imheight = size(dicom_image,1);
  
  theta = -atan(delta)*180/3.14159;
  xpivot = size(dicom_image,2);
  
  i = 0;
  s = 0;
  while xmin + s - 1 < imwidth
    s = i*delta;
    breast(ymin+i,ceil(xmin+s):imwidth) = 0;
%     xpivot = ceil(xmin+s);
    ypivot = ymin+i;
    i = i+1;
    if ymin+i > imheight
%       xpivot = ceil(xmin+s);
      break;
    end
  end
  
  for i = 1:ymin
    try
      s = i*delta;
      breast(ymin-i,ceil(xmin-s):imwidth) = 0;
      xpivot = ceil(xmin - s);
%       ypivot = ymin - i;
    catch ME
       break;
    end
  end
  
  dicom_image = [dicom_image,zeros(size(dicom_image,1),size(dicom_image,2)/4)];
  breast = [breast,zeros(size(dicom_image,1),size(dicom_image,2)/4)];
  distance_to_edge_map = [distance_to_edge_map,zeros(size(dicom_image,1),size(dicom_image,2)/4)];
  
  dicom_image = rotateAround(dicom_image,xpivot,ypivot,theta);
  breast = rotateAround(breast,xpivot,ypivot,theta);
  distance_to_edge_map = rotateAround(distance_to_edge_map,xpivot,ypivot,theta);
  end
end

% crop images so that only projected breast information is contained in each image
minrow = 1;
maxrow = size(breast,1);
mincol = 1;
maxcol = size(breast,2);
while minrow < size(breast,1);
  if sum(breast(minrow,:)) ~= 0
    break;
  end
  minrow = minrow + 1;
end
while maxrow > 1
  if sum(breast(maxrow,:)) ~= 0
    break;
  end
  maxrow = maxrow - 1;
end
while mincol < size(breast,1);
  if sum(breast(:,mincol)) ~= 0
    break;
  end
  mincol = mincol + 1;
end
while maxcol > 1
  if sum(breast(:,maxcol)) ~= 0
    break;
  end
  maxcol = maxcol - 1;
end

dicom_image = CropImage(dicom_image,minrow,maxrow,mincol,maxcol);
breast = CropImage(breast,minrow,maxrow,mincol,maxcol);
distance_to_edge_map = CropImage(distance_to_edge_map,minrow,maxrow,mincol,maxcol);


end
