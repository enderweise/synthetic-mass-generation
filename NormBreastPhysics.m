% Breast Normalization - Physics
function imdbl = NormBreastPhysics(imdbl,hdr)

% need to add exceptions for missing fields in header

% invert image to make pixel values proportional to x-ray intensity
if strcmp(hdr.PresentationLUTShape,'IDENTITY')
  [isocurve,breast_periphery,breast,breast_interior] = IsointensityCurve(imdbl,hdr);
  [imdbl,~,~,~] = InvertPixelData(imdbl,hdr);
  inverted = true;
elseif strcmp(hdr.PresentationLUTShape,'INVERSE')
  [dummy,~,~,~] = InvertPixelData(imdbl,hdr);
  [isocurve,breast_periphery,breast,breast_interior] = IsointensityCurve(dummy,hdr);
  inverted = false;
else
  inverted = false;
  warning(['Unknown Presentation LUT Shape: ',hdr.PresentationLUTShape])
end
breast_thickness_map = RawDicomThickness(breast_periphery,breast,breast_interior); % normalized, max val = 1
d = bwdist(1-breast); % distance from breast edge
dummy = bwdist(breast_interior); % distance from interior
D = d+dummy; % total distance from edge to interior and 'passing through' point (r,c)


% find attenuation coefficient
% load saved attenuation coefficients: two column vectors of equal length
  % Voltage is vector containing tube voltages in kev
  % lin_atten_coeff is vector containing linear coefficients of adipose tissue at corresponding x-ray energy in 1/cm
load('adiposeAttenuationCoeff.mat');
kvp = hdr.KVP;
i = find(double(kvp < Voltage),1);
if (kvp - Voltage(i-1)) < (Voltage(i) - kvp)
    i = i - 1;
end
mu = lin_atten_coeff(i);


% compressed breast height
H = hdr.BodyPartThickness; % stored in mm
H = H/10; % now in cm

mu = (-1)*mu*H; % now mu is unitless, like the values for breast thickness. Decreases number of flops
mu = mu*2;

% correction function, which increases the value of mu as the distance from the breast edge increases.
% mu(x) = mu*2*sqrt((d+delta)/(D+delta))
%   d is the distance from the edge in pixels
%   D is the distance to the breast interior (portion of breast that is
%   compressed) 
%   delta is a term minimizing effect of correction function
delta = 15; %pixels


% decrease pixel values (∝ to xray intensity) in breast by a factor of: exp( -mu_fat* [H- h(r)] )
[r,c] = find(breast == 1);
for k = 1:size(r,1)
  imdbl(r(k),c(k)) = imdbl(r(k),c(k))* exp( mu* (1- breast_thickness_map(r(k),c(k)))*...
     (sqrt((d(r(k),c(k))+delta)/(D(r(k),c(k))+delta))+1)/2 );
end

% un-invert image
if inverted
  [imdbl,~,~,~] = InvertPixelData(imdbl,hdr);
end

end
