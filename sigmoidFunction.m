function g = sigmoidFunction(g,maxOut,minOut,center,width)


if ~isa(g,'double')
  g = double(g);
end

if max(size(g)) == 1
  g = main(g,WindowCenter,WindowWidth,maxOutputIntensity,minOutputIntensity);
elseif min(size(g)) == 0
  warning('Empty variable was passed to dicomSigmoid')
else
  for i = 1:size(g,1)
    for j = 1:size(g,2)
      g(i,j) = main(g(i,j),maxOut,minOut,center,width);
    end % for - loop over rows of g
  end % for - loop over cols of g
end % conditional for size of g

end % function - dicomSigmoid


% 0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0
function g = main(g,gmax,gmin,A,B)

% apply sigmoid to double value of g
g = (gmax-gmin)/(1+exp((A-g)/B)) + gmin;

end % function - main()
