function [isocurve,breast_periphery,total_breast,breast_interior,hdr] = IsointensityCurve(img,hdr)

% Function that finds all the iso intensity curves in aN unprocessed mammogram
% Takes input of "FOR PROCESSING" dicom, the dicom header information,
% and the bin width of pixel intensities. 
% Intended for use on dicoms with PresentationIntentType 'FOR PROCESSING'.
% last updated:
  % 2015-12-10
  % Eric Weise

%scale_factor = 2;
H = size(img,1);
W = size(img,2);
%binwidth = floor(max(max(img))/50);
binwidth = 200;

if ~isa(img,'double')
  img = double(img);
end

% if the pixel values need to be inverted then invert them.
try
  LUTtransform = hdr.PresentationLUTShape;
catch ME
  PrintMissingInfo('Presentation LUT Shape for ',hdr.SOPInstanceUID,'during IsointensityCurves')
end

if strcmp(hdr.PresentationLUTShape,'INVERSE')
  [img,hdr,bd,maxpixelval] = InvertPixelData(img,hdr);
else
  try bd = hdr.BitDepth;
  catch ME
    try bd = hdr.BitsStored;
    catch ME
      m = double(max(max(img)));
      bd = 0;
      while m > 1
        m=m/2;
        bd=bd+1;
      end
    end
  end
  maxpixelval = double(2^bd-1);
end
if maxpixelval > 0

    
% [total_breast,H1,H2,W1] = BreastParameters(img,bd);
% img = img.*total_breast;

[total_breast,level] = FindBreastProjection(img,hdr);
H1 = 0; H2 = 0; W1 = 1;
for k = 1:H
  if ~isempty(find(total_breast(k,:),1))
    H1 = k;
    break;
  end
end
for k = H:-1:1
  if ~isempty(find(total_breast(k,:),1))
    H2 = k;
    break;
  end
end
for k = W:-1:1
  if isempty(find(total_breast(:,k),1))
    W1 = k;
    break;
  end
end

img = img.* total_breast;


p = maxpixelval;
while p >= level
  isocurve = double( img <= p );
  dummy = double( img > p-binwidth);
  isocurve = isocurve.*dummy;
  [cont,error] = IsointensityConditions(H,H1,H2,W,W1,isocurve);
  if cont
    break;
  end
%   imwrite(isocurve,['output/',hdr.SOPInstanceUID,'_0SecondToLast_','.png'])
  p = p - round(binwidth/2);
end
% imwrite(isocurve,['output/',hdr.SOPInstanceUID,'_1Isocurve_AllPieces_',num2str(bd),'.png'])

% Filter out crap from isocurve
isosum = sum(sum(isocurve));
for d = 1:10
  dummy = double(bwareafilt(logical(isocurve),d));
  if sum(sum(dummy)) > 0.85* isosum
    isocurve = dummy;
    break;
  end
end
% imwrite(isocurve,['output/',hdr.SOPInstanceUID,'_Isocurve_',num2str(bd),'.png'])

not_breast_interior = imdilate(double(img < p),ones(1,15));
breast_interior = 1-not_breast_interior;

breast_periphery = not_breast_interior.*total_breast;
% imwrite(breast_periphery,['output/',hdr.SOPInstanceUID,'_3BreastPeriphery_',num2str(bd),'.png'])

end

end

%% ------------------------------------------------------------------------------------------------------------------------------------------
function [total_breast,H1,H2,W1] = BreastParameters(img,bd)

% Create binary map of the following:
  % total_breast - a map of 1 for pixel containing breast information, 0 for background or 'RMLO' marker (e.g.)
  % 


% Otsu's thresholding method for finding the breast projection
[level,em] = graythresh(img);

if ( ~isfinite(level)  ||  isnan(level)  ||  level == 0 )
    level = level*double(2^bd);
    total_breast = double(zeros(size(img)));
    for r = 1:size(img,1)
        for c = 1:size(img,2)
            if img(r,c) > level
                total_breast(r,c) = 1;
            end
        end
    end
    total_breast = double(bwareafilt(logical(total_breast),1));
    
%otherwise use imdilate method for finding breast area
else
    threshold = 0;
    H = size(img,1);
    W = size(img,2);
    total_breast = imdilate(double(img <= threshold),ones(15));
    total_breast = (total_breast-1)*(-1);
    total_breast = double(bwareafilt(logical(total_breast),1));
    % imshow(total_breast)

    refr = sum(sum(total_breast));
    if refr > 0.8*H*W
      while threshold < 4001
        threshold = threshold + 200;
        total_breast = imdilate(double(img <= threshold),ones(15));
        total_breast = 1-total_breast;
        total_breast = double(bwareafilt(logical(total_breast),1));
        % imshow(total_breast)
        refr2 = sum(sum(total_breast));
        if refr*0.98 > refr2
          refr = refr2;
        else
          break;
        end
      end
    end
end

% return parameters useful for isointensity curve only
H1 = 0; H2 = 0; W1 = 1;
for k = 1:H
  if ~isempty(find(total_breast(k,:),1))
    H1 = k;
    break;
  end
end
for k = H:-1:1
  if ~isempty(find(total_breast(k,:),1))
    H2 = k;
    break;
  end
end
for k = W:-1:1
  if isempty(find(total_breast(:,k),1))
    W1 = k;
    break;
  end
end

end

%% ------------------------------------------------------------------------------------------------------------------------------------------
function [cont,error] = IsointensityConditions(H,H1,H2,W,W1,curvemap)

lwfactor1 = 10;
% cmdist = ((H+W)/8)^2;
cont = true;
error = 0;

% WIDTH CONDITION
M = 0;
for i = 1:W
  if ~isempty(find(curvemap(:,i),1,'first'))
    M = M + 1;
  end
end

% Condition - total width
if 3*M < (W-W1)
  cont = false;
  error = 1;
end

% CONDITION ON AVERAGE WIDTH SPACING OF PIXELS IN INTENSITY BIN
% HEIGHT CONDITION
if cont
  arr = zeros(H,1);
  avg_width = 0;
  N = 0;
  if find(curvemap,1,'first')
    for i = 1:H
      if max(curvemap(i,:)) == 1
        arr(i) = find(curvemap(i,:),1,'last') - find(curvemap(i,:),1,'first');
        N = N + 1;
      end
    end
    avg_width = sum(arr)/N;
  end

  % Condition - avg_width
  if avg_width*lwfactor1 > W
    cont = false;
    error = 2;
  % Condition - height
  elseif 4*N < 3*(H2-H1)
    cont = false;
    error = 3;
  end
end

% CONDITION DIFFERENT OBJECTS
if cont
  [objR,objC,~] = find(sparse(bwareafilt(logical(curvemap),1)));
  
  % Condition
  if ( 4*(max(objR)-min(objR)) < 3*abs(H2-H1)  ||  (4*max(objC)-min(objC)) < 3*(W-W1) )
    cont = false;
    error = 4;
  end
end
  

% % CONDITION CENTER OF MASS
% if cont
%   Nr = zeros(H,1); Nc = zeros(W,1);
%   centmassR = 0; centmassC = 0;
%   for l = 1:H
%     Nr(l) = sum(curvemap(l,:));
%     centmassR = centmassR + l*Nr(l);
%   end
%   for l = 1:W
%     Nc(l) = sum(curvemap(:,l));
%     centmassC = centmassC + l*Nc(l);
%   end
%   % find center of mass height and column
%   centmassR = centmassR/sum(Nr);
%   centmassC = centmassC/sum(Nc);
%   
%   %condition
%   ColCent = (W+W2)/2;
%   if (ColCent-centmassC)^2 + (RowCent-centmassR)^2 > cmdist
%     cont = false;
%   end
% end

% PROXIMITY TO EDGE CONDITION
% if cont
%   inner = 0;
%   outer = 0;
%   for l = 1:H
%     outer = outer + sum(sum(curvemap(l,outer_breast(l):inner_breast(l))));
%     inner = inner + sum(sum(curvemap(l,inner_breast(l):W)));
%   end
%   if inner*10 > outer
%       cont = false;
%   end
% end

end

%% ------------------------------------------------------------------------------------------------------------------------------------------
function [] = PrintMissingInfo(missing_tag,SOPinstanceUID,current_script)

if ( ischar(missing_tag)  &&  ischar(SOPinstanceUID)  &&  ischar(current_script) )
  sprintf('Missing tag in dicom header for ' + missing_tag + ' in image SOP Instance UID: ' + SOPinstanceUID + 'recorded in: ' + current_script + '()' )
else
  sprintf('Inputs to PrintMissingInfo() must be character strings')
end

end


% ------------------------------------------------------------------------------------------------------------------------------------------      
