function normalImage = addMassHeightToNormalImage(normalImage,hdr,lesionHeightCrop,cropname,normname)
cropsplit = strsplit(cropname,'_');
patientID = strsplit(cropsplit{1},'M');
cropSrcName = ['M' patientID{2} '_' cropsplit{6} '_' cropsplit{7} '_' patientID{1}];
[~,normalSrcName,~] = fileparts(normname);

if ~isa(normalImage,'double')
    normalImage = double(normalImage);
end

% Convert lesionHeightCrop to raw dicom, normalized to the clean breast
[isocurve,~,~,~,~] = IsointensityCurve(normalImage,hdr);
fattyTissueIntensity = sum(sum(normalImage.*isocurve))/sum(sum(isocurve));
convertedLesionCrop = zeros(size(lesionHeightCrop));
mu = getAttenuationCoefficients(hdr);
for r = 1:size(lesionHeightCrop,1)
    for c = 1:size(lesionHeightCrop,2)
        convertedLesionCrop(r,c) = fattyTissueIntensity*exp(-mu*lesionHeightCrop(r,c));
    end % for loop over columns of crops
end % for loop over rows of crops


% Create an array the same dimension as lesion crop that encodes the mixing weight at for each pixel in the crop
% Weighting array is essentially a PDF for the probability that a pixel is part of the lesion. Variables are:
% 1 - distance of pixel from 'edge' of the crop
% 2 - height of fibroglandular tissue in the pixel.
distArray = zeros(size(lesionHeightCrop));
r = round(min(size(distArray))/3);
for i = 1:r
    for j = 1:r-sqrt(i*(2*r-i))
        distArray(i,j) = 1;
        distArray(size(distArray,1)-i+1,j) = 1;
        distArray(i,size(distArray,2)-j+1) = 1;
        distArray(size(distArray,1)-i+1,size(distArray,2)-j+1) = 1;
    end % iterate over corner cols
end % iterate over corner rows
distArray(1,:) = 1;
distArray(:,1) = 1;
distArray(end,:) = 1;
distArray(:,end) = 1;
distArray = bwdist(distArray);
distProb = sigmoidFunction(distArray,1,0,40,20);

heightProb = zeros(size(lesionHeightCrop));
M = max(max(lesionHeightCrop));
heightProb = sigmoidFunction(lesionHeightCrop,1,0,M/3,M/8);

probOfMass = heightProb.*distProb;

% augment mass
[stray,mr,mc] = augmentCrop(convertedLesionCrop,probOfMass);

% graphically choose a location for the lesion to be inputted
[x,y] = getInsertCoordinates(normalImage,...
    size(normalImage,2)-mr,...
    size(normalImage,1)-mc,...
    mr,mc);

fid = fopen('cropBoundingBoxes.txt','a');
fprintf(fid,[cropSrcName,normalSrcName,' ',num2str(x),' ',num2str(y),' ',num2str(mc),' ',num2str(mr),char(10)']);
fclose(fid);
% Add lesion intensity information for converted lesion crop to the full normal image
%Mnormal = max(max(normalImage));
%Mheight = max(max(lesionHeightCrop));
for l = 1:max(size(stray))
    normImg = normalImage;
    crop = stray(l).crop;
    prob = stray(l).prob;
    epsilon = stray(l).ceps;
    for r = 1:size(crop,1)
        for c = 1:size(crop,2)
            normImg(r+y,c+x) = prob(r,c)*crop(r,c)*(1-epsilon) + (1-prob(r,c))*normImg(r+y,c+x);
        end % for loop over columns of crops
    end % for loop over rows of crops
    [processedImage,phdr] = ProcessRawDicom(normImg,hdr);
    imwrite(uint16(normImg),[cropSrcName,normalSrcName,'_',num2str(l),'_raw.png'])
    imwrite(uint16(processedImage)*16,[cropSrcName,normalSrcName,'_',num2str(l),'_processed.png'])
end % for loop over entries in stray

savehdr = [cropSrcName,normalSrcName,'_hdr'];
save(char(savehdr),'phdr');

end % function - addMassHeightToNormalImage()


%% 0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0
function [x,y] = getInsertCoordinates(normalImage,maxx,maxy,cropwidth,cropheight)

imshow(normalImage/max(max(normalImage))); hold on;

cont_tf = false;

while ~cont_tf
    % draw line showing where user can click
    line([maxx,maxx,1],...
        [1,maxy,maxy],...
        'color','red')
    [x,y] =ginput(1);
    x = floor(x);
    y = floor(y);
    if( x < maxx  &&  y < maxy )
        line([x,x,x+cropwidth,x+cropwidth,x],[y,y+cropheight,y+cropheight,y,y],'color','blue')
        choice = input('Is the crop box good? (y/n): ','s');
        if( choice == 'y'  ||  choice == 'Y' )
            hold off;
            break;
        else
            hold off;
            imshow(normalImage/max(max(normalImage)));
            hold on;
            clear x y
        end % conditional
    end % conditional
end % while loop

end % function - getInsertCoordinates()


%% 0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0
function mu = getAttenuationCoefficients(header)

% Grab relevant information from the dicom header
anode_material = header.AnodeTargetMaterial;
filter_material = header.FilterMaterial;
tube_voltage = round(header.KVP);
breast_thickness = header.BodyPartThickness; % units = mm
breast_thickness = round(breast_thickness/10); % convert breast thickness to cm and round

% retrieve the correct attenuation coefficients from the tables in this file --RETIRED April 4, 2016
if ( strcmp(filter_material,'MOLYBDENUM')  &&  strcmp(anode_material,'MOLYBDENUM') )
    mu = csvread('tables/MoMo.csv',tube_voltage-24,breast_thickness-2,[tube_voltage-24,breast_thickness-2,tube_voltage-24,breast_thickness-2]);
end

if ( strcmp(filter_material,'RHODIUM')  &&  strcmp(anode_material,'MOLYBDENUM') )
    mu = csvread('tables/MoRh.csv',tube_voltage-24,breast_thickness-2,[tube_voltage-24,breast_thickness-2,tube_voltage-24,breast_thickness-2]);
end

if ( strcmp(filter_material,'RHODIUM')  &&  strcmp(anode_material,'RHODIUM') )
    mu = csvread('tables/RhRh.csv',tube_voltage-24,breast_thickness-2,[tube_voltage-24,breast_thickness-2,tube_voltage-24,breast_thickness-2]);
end

end % function - getAttenuationCoefficients()

%% 0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0
function [stray,mr,mc] = augmentCrop(crop,probMap)

% randomly rotate and resize the crop and probability map 12 times

mc = 0; mr = 0;

ang  = 360*rand(12,1);
scl  = 0.5*rand(12,1)+0.75;
ceps = 0.1*rand(12,1);

for i = 1:12
    stray(i).crop = imresize(imrotate(crop,ang(i)),scl(i));
    stray(i).prob = imresize(imrotate(probMap,ang(i)),scl(i));
    stray(i).ceps = ceps(i);
    if size(stray(i).crop,1) > mr
        mr = size(stray(i).crop,1);
    end
    if size(stray(i).crop,2) > mc
        mc = size(stray(i).crop,2);
    end
end

end

%% 0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0
%function mixRatios = makeMixingRatio(height,width)
%
%dummy = zeros(height,width);
%
%dummy(1,:)   = 1;
%dummy(end,:) = 1;
%dummy(:,1)   = 1;
%dummy(:,end) = 1;
%
%mixingRatios = bwdist(dummy);
%
%end % function - makeMixingRatio()