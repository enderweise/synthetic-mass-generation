function breast_thickness_map = RawDicomThickness(breast_periphery,breast_projection_map,breast_interior)

distance_from_edge = bwdist(~logical(breast_projection_map));
distance_from_interior = bwdist(logical(breast_interior));
% set thickness values in breast interior
breast_thickness_map = breast_interior;
% set thickness values in breast periphery
for r = 1:size(breast_periphery,1)
    for c = 1:size(breast_periphery,2)
        if breast_periphery(r,c)
            D = distance_from_edge(r,c) + distance_from_interior(r,c);
            breast_thickness_map(r,c) =...
                sqrt(distance_from_edge(r,c)* ( 2*D- distance_from_edge(r,c) ))/D;
        end
    end
end

end
